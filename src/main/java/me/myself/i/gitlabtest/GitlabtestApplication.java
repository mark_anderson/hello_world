package me.myself.i.gitlabtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabtestApplication.class, args);
	}

}
